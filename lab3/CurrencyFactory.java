package lab3;

class CurrencyFactory{
    public static Currency getCurrency(String country) throws Exception
    {
	if(country.toLowerCase().equals("canada")){
	    return new CANDollar();
	}else if(country.toLowerCase().equals("usa")){
	    return new USDollar();
	}else if(country.toLowerCase().equals("mexico")){
	    return new Peso();
	}else{
	    throw new Exception("Currency not found!");
	}
    }
}
