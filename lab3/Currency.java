package lab3;

interface Currency{
    String getSymbol();
}

class Peso implements Currency{
    @Override
    public String getSymbol(){
	return "MXN";
    }
}

class CANDollar implements Currency{
    @Override
    public String getSymbol(){
	return "CAN";
    }
}

class USDollar implements Currency{
    @Override
    public String getSymbol(){
	return "USD";
    }
}
