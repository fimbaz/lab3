package lab3;

import java.util.Scanner;
/**
 * Created with IntelliJ IDEA.
 * User: Christopher Callan  & Matt Baker
 * Date: 2/14/13
 * Time: 6:21 PM
 * To change this template use File | Settings | File Templates.
 */
public class CurrencyFactoryDemo
{
    public static void main(String args[])
    {
        CurrencyFactory fact = new CurrencyFactory();
        Currency coin= null ;
        Scanner kybd = new Scanner(System.in);
        System.out.print("What currency would you like to produce? ");
        boolean caught;
        do
        {
            try
            {
                coin = fact.getCurrency(kybd.nextLine());
                caught=false;
            }
            catch(Exception e)
            {
                System.out.println("Please try again.");
                caught=true;
            }
        }while(caught);
        System.out.println(coin.getSymbol());
    }

}
